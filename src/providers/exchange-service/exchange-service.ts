import { Observable } from 'rxjs/Observable';
//import { Data } from './../../models/data';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ExchangeServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ExchangeServiceProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ExchangeServiceProvider Provider');
  }

  getExchange(q: number, from: string, to: string): Observable<any> {
    let url = "https://currency-exchange.p.rapidapi.com/exchange?q=" + q + "&from=" + from + "&to=" + to;
    const headers = new HttpHeaders({'X-RapidAPI-Key': '80fd63cdb1mshe55760448b8b221p1b4265jsnf3a8eb2b2b5e'});
    return this.http.get<any>(url, {headers});
  }

}

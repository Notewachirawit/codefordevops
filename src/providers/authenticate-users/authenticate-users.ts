import { AngularFireAuth } from '@angular/fire/auth';
import { GENARAL } from '../../app/_centralized/_configuration/genaral';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'firebase';

@Injectable()
export class AuthenticateUsersProvider {

  constructor(private angularFireAuth: AngularFireAuth) { }

  createUserWithUsernameAndPassword(username: string, password: string): Promise<firebase.auth.UserCredential> {
    username += GENARAL.SUFFIXES_USERNAME;
    return this.angularFireAuth.auth.createUserWithEmailAndPassword(username, password);
  }

  signInWithUsernameAndPassword(username: string, password: string): Promise<firebase.auth.UserCredential> {
    username += GENARAL.SUFFIXES_USERNAME;
    return this.angularFireAuth.auth.signInWithEmailAndPassword(username, password);
  }

  informationAuthState(): Observable<User> {
    return this.angularFireAuth.authState;
  }

  signOut(): Promise<void> {
    return this.angularFireAuth.auth.signOut();
  }
  
}

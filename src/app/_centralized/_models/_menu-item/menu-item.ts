import { DetailImage } from './../_menu-item/detail-image';
export interface MenuItem {
    price: number;
    unit: string;
    useFlag: boolean;
    detailImage: DetailImage;
    outOfStock: boolean;
}

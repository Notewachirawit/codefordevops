export interface DatasMenu {    
    name: string;
    imgPath: string;
    imgUrl: string;
    price: number;
    unit: string;
    useFlag: boolean;
    outOfStock: boolean;
    amount: number;
}
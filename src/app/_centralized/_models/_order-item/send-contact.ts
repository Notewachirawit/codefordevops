import { Profile } from "../_users/profile";

export interface SendContact {
    uidUser: string;
    profile: Profile;
}

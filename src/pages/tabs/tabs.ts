//import { GENARAL } from '../../app/_centralized/_configuration/genaral';
import { Data } from './../../models/data';
import { ExchangeServiceProvider } from './../../providers/exchange-service/exchange-service';
import { NavController, AlertController} from 'ionic-angular';
import { Component } from '@angular/core';
import { SignInPage } from '../sign-in/sign-in';
//import { GENARAL } from '../../app/_centralized/_configuration/genaral';
//import { NavController, AlertController } from 'ionic-angular';

@Component({
  
  templateUrl: 'tabs.html'
})
export class TabsPage {

  constructor(private navCtrl: NavController, public exchangeService: ExchangeServiceProvider, public alertCtrl: AlertController) {

  }
  
  to: string;
  from: string;
  value: number;
  data: Data;
  exchangeValue: any;
  _GENARAL: any;
  showAlert(title: string, message: string) {
    let alert = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: ['ตกลง']
    });
    alert.present()
  }
  changePageToSignIn(): void {
    const title = '';
  
   this.showAlert(title, 'logout success');
   //this.navCtrl.setRoot(this.navCtrl.getActive().component);
   this.navCtrl.setRoot(SignInPage);
   window.location.reload()
  }
  getExchange() {
    if (!this.to && !this.from) {
      this.alertCtrl.create({
        title: "Warnning",
        subTitle: "Please fill to and from value ",
        buttons: ["OK"]
      }).present();
    } else if (!this.to) {
      this.alertCtrl.create({
        title: "Warnning",
        subTitle: "Please fill to value",
        buttons: ["OK"]
      }).present();
    } else if (!this.from) {
      this.alertCtrl.create({
        title: "Warnning",
        subTitle: "Please fill from value",
        buttons: ["OK"]
      }).present();
    } else if (!this.value) {
      this.alertCtrl.create({
        title: "Warnning",
        subTitle: "Please fill from value",
        buttons: ["OK"]
      }).present();
    } else if (this.value <= 0) {
      this.alertCtrl.create({
        title: "Warnning",
        subTitle: "Please fill value over 0 ",
        buttons: ["OK"]
      }).present();
    } else {
      this.exchangeService.getExchange(this.value, this.from, this.to).subscribe(response => {
        console.log(JSON.stringify(response));
        this.exchangeValue = response;
      }, error => {
        console.error(error);
      });
    }
  }


}
